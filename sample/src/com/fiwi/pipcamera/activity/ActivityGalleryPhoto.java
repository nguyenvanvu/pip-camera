package com.fiwi.pipcamera.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fiwi.pipcamera.R;
import com.fiwi.pipcamera.common.MyApplication;
import com.fiwi.pipcamera.model.TouchImageView;
import com.fiwi.pipcamera.tools.GPUImageFilterTools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;

public class ActivityGalleryPhoto extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener,
        View.OnClickListener, GPUImageView.OnPictureSavedListener {
    private final String TAG = "ActivityGallery";
    private final String ACTION_BACKGROUND = "background";
    private final String ACTION_FOREGROUND = "foreground";
    private final String ACTION_PIP = "pip";
    private final String ACTION_CHANGE_BG = "changebg";
    private final String ACTION_CHANGE_FG = "changefg";
    private Toolbar toolBar;
    private ImageView imgBack;
    private TextView txtSave;
    private LinearLayout lnPIP;
    private LinearLayout lnFrame;
    private LinearLayout lnFilterBg;
    private LinearLayout lnFilterFG;
    private LinearLayout lnChangeBg;
    private LinearLayout lnListFilterType;
    private LinearLayout lnChangeBgAndFg;
    private RecyclerView rcFilterTypePhoto;
    private static final int REQUEST_PICK_IMAGE_INPUT = 1;
    private static final int REQUEST_PICK_BACKGROUND = 2;
    private static final int REQUEST_PICK_FOREGROUND = 3;
    private GPUImageFilter mFilterBg;
    private GPUImageFilter mFilterFg;
    private GPUImageFilterTools.FilterAdjuster mFilterAdjuster;
    private GPUImageView mGPUImageView;
    private ImageView imgBg;
    private ImageView imgFrame;
    private ImageView txtChangeBg;
    private ImageView txtChangeFg;
    private TouchImageView imgThumnail;
    private Button btnChooseImage;
    private Button btnSave;
    private Bitmap bitmapFrame;
    private Bitmap bitmapBg;
    private Bitmap bitmapFrameFixed;
    private Bitmap bitmapBgFixed;
    private Bitmap bitmapEmptyBg;
    private Bitmap bitmapFrameThumnail;
    private Bitmap bitmapFrameThumnailFixed;
    private Bitmap bitmapEmptyThumnailFixed;
    private Bitmap bitmapBgThumnailFixed;
    private Bitmap bitmapParentToSave;
    private float scaleDip;
    private int widthScreen;
    private int heightScreen;
    private float scaleLeftFrameThumnail;
    private float scaleTopFrameThumnail;
    private float scaleFrameBeginWithFixed;
    private int widthFrameFixed;
    private int heightFrameFixed;
    private int marginLeftFrameThumnail;
    private int marginTopFrameThumnail;
    private int widthBitmapFrameThumnailFixed;
    private int heightBitmapFrameThumnailFixed;
    private Canvas mCanvasBitmapParentToSave;
    private Paint mPaintBitmapParentToSave;
    private Bitmap bitmapThumnailToSave;
    private boolean isZoomed;
    public Bitmap bitmapInputToFilter;
    private InputStream imageStream;
    private GPUImageFilterTools gpuImageFilterTools;
    private LinearLayoutManager manager;
    private String checkAction = "pip";
    private Bitmap bitmapThumnailBgBefore;
    private Bitmap bitmapBgBefore;
    private GPUImage mGpuImage;
    private Bitmap bitmapBgChanged;
    private Bitmap bitmapFgChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_photo);
        initView();
        getImageInputFromeGllery();
    }

    public void initView() {
        toolBar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolBar);
        imgFrame = (ImageView) findViewById(R.id.imgFrame);
        imgBg = (ImageView) findViewById(R.id.imgBg);
        imgThumnail = (TouchImageView) findViewById(R.id.imgThumnail);
        bitmapFrame = BitmapFactory.decodeResource(getResources(), R.drawable.framebong);
        bitmapFrameThumnail = BitmapFactory.decodeResource(getResources(), R.drawable.thumnailbong);
        mGPUImageView = (GPUImageView) findViewById(R.id.gpuimage);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        txtSave = (TextView) findViewById(R.id.txtSave);
        lnPIP = (LinearLayout) findViewById(R.id.lnPIP);
        lnFrame = (LinearLayout) findViewById(R.id.lnFrame);
        lnFilterBg = (LinearLayout) findViewById(R.id.lnFilterBg);
        lnFilterFG = (LinearLayout) findViewById(R.id.lnFilterForeBg);
        lnChangeBg = (LinearLayout) findViewById(R.id.lnChangeBg);
        lnListFilterType = (LinearLayout) findViewById(R.id.lnListFilterType);
        lnChangeBgAndFg = (LinearLayout) findViewById(R.id.lnChangeBgAndFg);
        txtChangeBg = (ImageView) findViewById(R.id.txtChangeBg);
        txtChangeFg = (ImageView) findViewById(R.id.txtChangeFg);
        imgFrame = (ImageView) findViewById(R.id.imgFrame);
        imgBg = (ImageView) findViewById(R.id.imgBg);
        imgThumnail = (TouchImageView) findViewById(R.id.imgThumnail);
        bitmapFrame = BitmapFactory.decodeResource(getResources(), R.drawable.framebong);
        bitmapFrameThumnail = BitmapFactory.decodeResource(getResources(), R.drawable.thumnailbong);
        lnPIP.setOnClickListener(this);
        lnFrame.setOnClickListener(this);
        lnFilterBg.setOnClickListener(this);
        lnFilterFG.setOnClickListener(this);
        lnChangeBg.setOnClickListener(this);
        txtChangeBg.setOnClickListener(this);
        txtChangeFg.setOnClickListener(this);
        rcFilterTypePhoto = (RecyclerView) findViewById(R.id.rcFilterTypePhoto);
        setSupportActionBar(toolBar);
        initListFilterType();

    }

    public void initListFilterType() {
        manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rcFilterTypePhoto.setLayoutManager(manager);
        gpuImageFilterTools = new GPUImageFilterTools();
        gpuImageFilterTools.showListFilter(ActivityGalleryPhoto.this, new GPUImageFilterTools.OnGpuImageFilterChosenListener() {
            @Override
            public void onGpuImageFilterChosenListener(final GPUImageFilter filter) {
//                switchFilterTo(filter);
//                mGPUImageView.requestRender();
//                bitmapInputToFilter = BitmapFactory.decodeStream(imageStream);
                if (checkAction.equals(ACTION_BACKGROUND)) {
                    mFilterBg = filter;
                } else if (checkAction.equals(ACTION_FOREGROUND)) {
                    mFilterFg = filter;
                }
                mGpuImage = new GPUImage(ActivityGalleryPhoto.this);
                mGpuImage.setFilter(filter);//Your filter here
                bitmapBg = mGpuImage.getBitmapWithFilterApplied(bitmapInputToFilter);
                if(bitmapBgChanged!=null){
                    bitmapBg = mGpuImage.getBitmapWithFilterApplied(bitmapBgChanged);
                }else if(bitmapFgChanged!=null){
                    bitmapBg = mGpuImage.getBitmapWithFilterApplied(bitmapFgChanged);
                }
                handlingImage();
            }
        }, rcFilterTypePhoto);
    }

    public void handlingImage() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        widthScreen = metrics.widthPixels;
        heightScreen = metrics.heightPixels;
        scaleDip = getResources().getDisplayMetrics().density;
        scaleLeftFrameThumnail = (float) 176 / (float) bitmapFrame.getWidth() * scaleDip;
        scaleTopFrameThumnail = (float) 40 / (float) bitmapFrame.getHeight() * scaleDip;
        scaleFrameBeginWithFixed = (float) (widthScreen) / (float) bitmapFrame.getWidth();
        widthFrameFixed = widthScreen;
        heightFrameFixed = (int) (scaleFrameBeginWithFixed * bitmapFrame.getHeight());
        marginLeftFrameThumnail = (int) (scaleLeftFrameThumnail * widthFrameFixed);
        marginTopFrameThumnail = (int) (scaleTopFrameThumnail * heightFrameFixed);
        widthBitmapFrameThumnailFixed = (int) (bitmapFrameThumnail.getWidth() * scaleFrameBeginWithFixed);
        heightBitmapFrameThumnailFixed = (int) (bitmapFrameThumnail.getHeight() * scaleFrameBeginWithFixed);
        if (checkAction.equals(ACTION_BACKGROUND)) {
            initBackground(bitmapBg);
            bitmapThumnailBgBefore = Bitmap.createScaledBitmap(bitmapThumnailBgBefore, bitmapBg.getWidth(), bitmapBg.getHeight(), false);
            bitmapBgBefore = Bitmap.createScaledBitmap(bitmapBg, bitmapBg.getWidth(), bitmapBg.getHeight(), false);
            initThumnail(bitmapThumnailBgBefore);
        } else if (checkAction.equals(ACTION_FOREGROUND)) {
            initThumnail(bitmapBg);
            bitmapThumnailBgBefore = Bitmap.createScaledBitmap(bitmapBg, bitmapBg.getWidth(), bitmapBg.getHeight(), false);
            bitmapBgBefore = Bitmap.createScaledBitmap(bitmapBgBefore, bitmapBg.getWidth(), bitmapBg.getHeight(), false);
            initBackground(bitmapBgBefore);
        } else if (checkAction.equals(ACTION_PIP)) {
            initBackground(bitmapBg);
            initThumnail(bitmapBg);
            bitmapThumnailBgBefore = Bitmap.createScaledBitmap(bitmapBg, bitmapBg.getWidth(), bitmapBg.getHeight(), false);
            bitmapBgBefore = Bitmap.createScaledBitmap(bitmapBg, bitmapBg.getWidth(), bitmapBg.getHeight(), false);
        } else if (checkAction.equals(ACTION_CHANGE_BG)) {
            initBackground(bitmapBgChanged);
        } else if (checkAction.equals(ACTION_CHANGE_FG)) {
            initThumnail(bitmapFgChanged);
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(marginLeftFrameThumnail, marginTopFrameThumnail, 0, 0);
        imgThumnail.setLayoutParams(layoutParams);
        imgFrame.setImageBitmap(bitmapFrameFixed);
        imgBg.setImageBitmap(bitmapEmptyBg);
        imgThumnail.setImageBitmap(bitmapEmptyThumnailFixed);
        imgThumnail.setZoom(1);
        imgThumnail.setOnTouchImageViewListener(new TouchImageView.OnTouchImageViewListener() {
            @Override
            public void onMove() {
                isZoomed = imgThumnail.isZoomed();
                imgThumnail.setDrawingCacheEnabled(true);
                imgThumnail.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                bitmapThumnailToSave = imgThumnail.getDrawingCache();
            }
        });
    }

    public void initBackground(Bitmap bitmapBgFiltered) {
        bitmapFrameFixed = Bitmap.createScaledBitmap(bitmapFrame, widthFrameFixed, heightFrameFixed, false);
        bitmapFrameThumnailFixed = Bitmap.createScaledBitmap(bitmapFrameThumnail, widthBitmapFrameThumnailFixed, heightBitmapFrameThumnailFixed, false);
        bitmapBgFixed = Bitmap.createScaledBitmap(bitmapBgFiltered, widthFrameFixed, heightFrameFixed, false);
        bitmapEmptyBg = Bitmap.createBitmap(widthFrameFixed, heightFrameFixed, Bitmap.Config.ARGB_8888);
        Canvas mCanvasBg = new Canvas(bitmapEmptyBg);
        Paint mPaintBg = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintBg.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR.DST_OUT));
        mCanvasBg.drawBitmap(bitmapBgFixed, 0, 0, null);
        mCanvasBg.drawBitmap(bitmapFrameThumnailFixed, marginLeftFrameThumnail, marginTopFrameThumnail, mPaintBg);
    }

    public void initThumnail(Bitmap bitmapThumnailFiltered) {
        bitmapEmptyThumnailFixed = Bitmap.createBitmap(widthBitmapFrameThumnailFixed, heightBitmapFrameThumnailFixed, Bitmap.Config.ARGB_8888);
        bitmapBgThumnailFixed = Bitmap.createScaledBitmap(bitmapThumnailFiltered, widthBitmapFrameThumnailFixed, heightBitmapFrameThumnailFixed, false);
        Canvas mCanvasThumnail = new Canvas(bitmapEmptyThumnailFixed);
        Paint mPaintThumnail = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCanvasThumnail.drawBitmap(bitmapBgThumnailFixed, 0, 0, mPaintThumnail);
    }

    public void save() {
        bitmapParentToSave = Bitmap.createBitmap(widthFrameFixed, heightFrameFixed, Bitmap.Config.ARGB_8888);
        mCanvasBitmapParentToSave = new Canvas(bitmapParentToSave);
        mPaintBitmapParentToSave = new Paint(Paint.ANTI_ALIAS_FLAG);
        if (isZoomed == true) {
            mCanvasBitmapParentToSave.drawBitmap(bitmapThumnailToSave, marginLeftFrameThumnail, marginTopFrameThumnail, mPaintBitmapParentToSave);
        } else {
            mCanvasBitmapParentToSave.drawBitmap(bitmapEmptyThumnailFixed, marginLeftFrameThumnail, marginTopFrameThumnail, mPaintBitmapParentToSave);

        }
        mCanvasBitmapParentToSave.drawBitmap(bitmapEmptyBg, 0, 0, mPaintBitmapParentToSave);
        mCanvasBitmapParentToSave.drawBitmap(bitmapFrameFixed, 0, 0, mPaintBitmapParentToSave);
        saveImage(bitmapParentToSave);
    }

    public void saveImage(Bitmap bitmapToSave) {
        Toast.makeText(this, "abc", Toast.LENGTH_SHORT).show();
        File dir = new File(Environment.getExternalStorageDirectory(), "PhysicsSketchpad");
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dir, "sketchpad" + ".png");
        if (file.exists())
            file.delete();
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bitmapToSave.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri uri;
            if (requestCode == REQUEST_PICK_IMAGE_INPUT) {
//                handleImage(data.getData());
                try {
                    uri = data.getData();
                    imageStream = getContentResolver().openInputStream(uri);
                    bitmapBg = BitmapFactory.decodeStream(imageStream);
                    bitmapInputToFilter = Bitmap.createScaledBitmap(bitmapBg, bitmapBg.getWidth(), bitmapBg.getHeight(), false);
                    handlingImage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_PICK_BACKGROUND) {
                try {
                    uri = data.getData();
                    imageStream = getContentResolver().openInputStream(uri);
                    bitmapBg = BitmapFactory.decodeStream(imageStream);
                    bitmapInputToFilter = Bitmap.createScaledBitmap(bitmapBg, bitmapBg.getWidth(), bitmapBg.getHeight(), false);
                    mGpuImage = new GPUImage(ActivityGalleryPhoto.this);
                    mGpuImage.setFilter(mFilterBg);//Your filter here
                    bitmapBg = mGpuImage.getBitmapWithFilterApplied(bitmapInputToFilter);
                    bitmapBgChanged = Bitmap.createScaledBitmap(bitmapBg,bitmapBg.getWidth(),bitmapBg.getHeight(),false);
                    handlingImage();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_PICK_FOREGROUND) {
                try {
                    uri = data.getData();
                    imageStream = getContentResolver().openInputStream(uri);
                    bitmapBg = BitmapFactory.decodeStream(imageStream);
                    bitmapInputToFilter = Bitmap.createScaledBitmap(bitmapBg, bitmapBg.getWidth(), bitmapBg.getHeight(), false);
                    mGpuImage = new GPUImage(ActivityGalleryPhoto.this);
                    mGpuImage.setFilter(mFilterFg);//Your filter here
                    bitmapBg = mGpuImage.getBitmapWithFilterApplied(bitmapInputToFilter);
                    bitmapFgChanged = Bitmap.createScaledBitmap(bitmapBg,bitmapBg.getWidth(),bitmapBg.getHeight(),false);
                    handlingImage();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void getImageInputFromeGllery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_PICK_IMAGE_INPUT);
    }

    public void pickChangeBackGround() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_PICK_BACKGROUND);
    }

    public void pickChangeForeGround() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_PICK_FOREGROUND);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 50:
                for (int i = 0, len = permissions.length; i < len; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale = shouldShowRequestPermissionRationale(permission);
                        if (!showRationale) {
                            showMessageOKCancel(ActivityGalleryPhoto.this, "You need to allow access to read storage!",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                                            intent.setData(uri);
                                            startActivityForResult(intent, 105);
                                            Toast.makeText(ActivityGalleryPhoto.this, "Please allow read external storage", Toast.LENGTH_SHORT)
                                                    .show();
                                            ActivityCompat.requestPermissions(ActivityGalleryPhoto.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                                    50);
                                        }
                                    });
                        }
                    }
                }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    50);
            return false;
        } else {
            return true;
        }

    }

    private static void showMessageOKCancel(Context context, String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        handlingImage();
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.myFilterTypes.clear();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lnPIP:
                lnPIP.setBackgroundColor(Color.parseColor("#232935"));
                lnChangeBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterFG.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFrame.setBackgroundColor(Color.parseColor("#1f2530"));
                break;
            case R.id.lnFrame:
                lnFrame.setBackgroundColor(Color.parseColor("#232935"));
                lnChangeBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterFG.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnPIP.setBackgroundColor(Color.parseColor("#1f2530"));
                break;
            case R.id.lnFilterBg:
                checkAction = ACTION_BACKGROUND;
                lnFilterBg.setBackgroundColor(Color.parseColor("#232935"));
                lnChangeBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterFG.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFrame.setBackgroundColor(Color.parseColor("#1f2530"));
                lnPIP.setBackgroundColor(Color.parseColor("#1f2530"));
                lnListFilterType.setVisibility(View.VISIBLE);
                lnChangeBgAndFg.setVisibility(View.GONE);
                break;
            case R.id.lnFilterForeBg:
                checkAction = ACTION_FOREGROUND;
                lnFilterFG.setBackgroundColor(Color.parseColor("#232935"));
                lnChangeBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFrame.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnPIP.setBackgroundColor(Color.parseColor("#1f2530"));
                lnListFilterType.setVisibility(View.VISIBLE);
                lnChangeBgAndFg.setVisibility(View.GONE);
                break;
            case R.id.lnChangeBg:
                lnChangeBg.setBackgroundColor(Color.parseColor("#232935"));
                lnFrame.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterFG.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnPIP.setBackgroundColor(Color.parseColor("#1f2530"));
                lnListFilterType.setVisibility(View.GONE);
                lnChangeBgAndFg.setVisibility(View.VISIBLE);
                break;
            case R.id.txtChangeBg:
                checkAction = ACTION_CHANGE_BG;
                pickChangeBackGround();
                break;
            case R.id.txtChangeFg:
                checkAction = ACTION_CHANGE_FG;
                pickChangeForeGround();
                break;
//            case R.id.button_choose_filter:
//                GPUImageFilterTools.showDialog(this, new GPUImageFilterTools.OnGpuImageFilterChosenListener() {
//
//                    @Override
//                    public void onGpuImageFilterChosenListener(final GPUImageFilter filter) {
//                        switchFilterTo(filter);
//                        mGPUImageView.requestRender();
//                    }
//
//                });
//                break;
//            case R.id.button_save:
//                saveImage();
//                break;
            default:
                break;
        }
    }

    @Override
    public void onPictureSaved(final Uri uri) {
        Toast.makeText(this, "Saved: " + uri.toString(), Toast.LENGTH_SHORT).show();
    }

    private void saveImage() {
        String fileName = System.currentTimeMillis() + ".jpg";
        mGPUImageView.saveToPictures("GPUImage", fileName, this);
//        mGPUImageView.saveToPictures("GPUImage", fileName, 1600, 1600, this);
    }

//    private void switchFilterTo(final GPUImageFilter filter) {
//        if (mFilter == null
//                || (filter != null && !mFilter.getClass().equals(filter.getClass()))) {
//            mFilter = filter;
//            mGPUImageView.setFilter(mFilter);
//            mFilterAdjuster = new GPUImageFilterTools.FilterAdjuster(mFilter);
////            findViewById(R.id.seekBar).setVisibility(
////                    mFilterAdjuster.canAdjust() ? View.VISIBLE : View.GONE);
//        }
//    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (mFilterAdjuster != null) {
            mFilterAdjuster.adjust(progress);
        }
        mGPUImageView.requestRender();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private void handleImage(final Uri selectedImage) {
        mGPUImageView.setImage(selectedImage);
    }
}
