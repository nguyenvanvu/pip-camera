/*
 * Copyright (C) 2012 CyberAgent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fiwi.pipcamera.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.fiwi.pipcamera.R;
import com.fiwi.pipcamera.lib_loop_viewpager.LoopViewPager;
import com.fiwi.pipcamera.lib_loop_viewpager.SamplePagerAdapter;

import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;


public class ActivityMain extends AppCompatActivity implements OnClickListener {
    private ImageView imgCamera;
    private ImageView imgAlbum;

    @Override public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkPermissionCamera(this);
        checkPermissionExternalStorage(this);
        initView();
    }

    private void initView(){
        imgCamera = (ImageView) findViewById(R.id.imgCamera);
        imgAlbum = (ImageView) findViewById(R.id.imgAlbum);
        imgCamera.setOnClickListener(this);
        imgAlbum.setOnClickListener(this);
        final LoopViewPager viewpager = (LoopViewPager) findViewById(R.id.viewPager);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
//        indicator.configureIndicator(13,13,5);
        viewpager.setAdapter(new SamplePagerAdapter());
        indicator.setViewPager(viewpager);
        final Handler handler = new Handler();
        final int[] currentPage = {viewpager.getCurrentItem()};
        final Runnable update = new Runnable() {
            public void run() {
                if (currentPage[0] == 5) {
                    currentPage[0] = 0;
                }
                viewpager.setCurrentItem(currentPage[0]++, true);
            }
        };


        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 3000, 3000);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgAlbum:
                checkPermissionExternalStorage(ActivityMain.this);
                startActivity(new Intent(this, ActivityGalleryPhoto.class));
                break;
            case R.id.imgCamera:
                checkPermissionCamera(ActivityMain.this);
                startActivity(new Intent(this, ActivityCamera.class));
                break;

            default:
                break;
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 50:
                for (int i = 0, len = permissions.length; i < len; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale = shouldShowRequestPermissionRationale(permission);
                        if (!showRationale) {
                            showMessageOKCancel(ActivityMain.this, "You need to allow access to read storage!",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                                            intent.setData(uri);
                                            startActivityForResult(intent, 105);
                                            Toast.makeText(ActivityMain.this, "Please allow read external storage", Toast.LENGTH_SHORT)
                                                    .show();
                                            ActivityCompat.requestPermissions(ActivityMain.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                                    50);
                                        }
                                    });
                        }
                    }
                }
            case 60:
                for (int i = 0, len = permissions.length; i < len; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale = shouldShowRequestPermissionRationale(permission);
                        if (!showRationale) {
                            showMessageOKCancel(ActivityMain.this, "You need to allow access to read storage!",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                                            intent.setData(uri);
                                            startActivityForResult(intent, 105);
                                            Toast.makeText(ActivityMain.this, "Please allow read external storage", Toast.LENGTH_SHORT)
                                                    .show();
                                            ActivityCompat.requestPermissions(ActivityMain.this, new String[]{Manifest.permission.CAMERA},
                                                    60);
                                        }
                                    });
                        }
                    }
                }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermissionExternalStorage(final Context context) {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    50);
            return false;
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermissionCamera(final Context context) {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA},
                    60);
            return false;
        } else {
            return true;
        }
    }

    private static void showMessageOKCancel(Context context, String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }



}
