/*
 * Copyright (C) 2012 CyberAgent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fiwi.pipcamera.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.fiwi.pipcamera.R;
import com.fiwi.pipcamera.common.MyApplication;
import com.fiwi.pipcamera.model.TouchImageView;
import com.fiwi.pipcamera.tools.GPUImageFilterTools;

import java.io.IOException;
import java.io.InputStream;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;
import jp.co.cyberagent.android.gpuimage.GPUImageView.OnPictureSavedListener;

import static com.fiwi.pipcamera.tools.GPUImageFilterTools.context;

public class ActivityGallery extends AppCompatActivity implements OnSeekBarChangeListener,
        OnClickListener, OnPictureSavedListener {
    private final String TAG = "ActivityGallery";
    private Toolbar toolBar;
    private ImageView imgBack;
    private TextView txtSave;
    private LinearLayout lnPIP;
    private LinearLayout lnFrame;
    private LinearLayout lnFilterBg;
    private LinearLayout lnFilterFG;
    private LinearLayout lnChangeBg;
    private LinearLayout lnListFilterType;
    private LinearLayout lnChangeBgAndFg;
    private RecyclerView rcFilterTypePhoto;
    private static final int REQUEST_PICK_IMAGE = 1;
    private GPUImageFilter mFilter;
    private GPUImageFilterTools.FilterAdjuster mFilterAdjuster;
    private GPUImageView mGPUImageView;
    ImageView imgBg;
    ImageView imgFrame;
    TouchImageView imgThumnail;
    Button btnChooseImage;
    Button btnSave;
    Bitmap bitmapFrame;
    Bitmap bitmapBg;
    Bitmap bitmapFrameFixed;
    Bitmap bitmapBgFixed;
    Bitmap bitmapEmptyBg;
    Bitmap bitmapFrameThumnail;
    Bitmap bitmapFrameThumnailFixed;
    Bitmap bitmapEmptyThumnailFixed;
    Bitmap bitmapBgThumnailFixed;
    Bitmap bitmapParentToSave;
    float scaleDip;
    int widthScreen;
    int heightScreen;
    float scaleLeftFrameThumnail;
    float scaleTopFrameThumnail;
    float scaleFrameBeginWithFixed;
    int widthFrameFixed;
    int heightFrameFixed;
    int marginLeftFrameThumnail;
    int marginTopFrameThumnail;
    int widthBitmapFrameThumnailFixed;
    int heightBitmapFrameThumnailFixed;
    Canvas mCanvasBitmapParentToSave;
    Paint mPaintBitmapParentToSave;
    Bitmap bitmapThumnailToSave;
    boolean isZoomed;
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_PICK_IMAGE);
        initView();
    }

    public void initView() {
        toolBar = (Toolbar) findViewById(R.id.toolBar);
//        ((SeekBar) findViewById(R.id.seekBar)).setOnSeekBarChangeListener(this);
        mGPUImageView = (GPUImageView) findViewById(R.id.gpuimage);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        txtSave = (TextView) findViewById(R.id.txtSave);
        lnPIP = (LinearLayout) findViewById(R.id.lnPIP);
        lnFrame = (LinearLayout) findViewById(R.id.lnFrame);
        lnFilterBg = (LinearLayout) findViewById(R.id.lnFilterBg);
        lnFilterFG = (LinearLayout) findViewById(R.id.lnFilterForeBg);
        lnChangeBg = (LinearLayout) findViewById(R.id.lnChangeBg);
        lnListFilterType = (LinearLayout) findViewById(R.id.lnListFilterType);
        lnChangeBgAndFg = (LinearLayout) findViewById(R.id.lnChangeBgAndFg);
        imgFrame = (ImageView) findViewById(R.id.imgFrame);
        imgBg = (ImageView) findViewById(R.id.imgBg);
        imgThumnail = (TouchImageView) findViewById(R.id.imgThumnail);
        bitmapFrame = BitmapFactory.decodeResource(getResources(), R.drawable.framebong);
        bitmapFrameThumnail = BitmapFactory.decodeResource(getResources(), R.drawable.thumnailbong);
        lnPIP.setOnClickListener(this);
        lnFrame.setOnClickListener(this);
        lnFilterBg.setOnClickListener(this);
        lnFilterFG.setOnClickListener(this);
        lnChangeBg.setOnClickListener(this);
        rcFilterTypePhoto = (RecyclerView) findViewById(R.id.rcFilterTypePhoto);
        setSupportActionBar(toolBar);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rcFilterTypePhoto.setLayoutManager(manager);
        GPUImageFilterTools gpuImageFilterTools = new GPUImageFilterTools();
        gpuImageFilterTools.showListFilter(ActivityGallery.this, new GPUImageFilterTools.OnGpuImageFilterChosenListener() {
            @Override
            public void onGpuImageFilterChosenListener(final GPUImageFilter filter) {
                switchFilterTo(filter);
//                mGPUImageView.requestRender();
                GPUImage mGpuImage = new GPUImage(context);
                mGpuImage.setFilter(filter);//Your filter here
                bitmapBg = mGpuImage.getBitmapWithFilterApplied(bitmapBg);
                handlingImage();
            }
        }, rcFilterTypePhoto);
    }

    public void handlingImage() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        widthScreen = metrics.widthPixels;
        heightScreen = metrics.heightPixels;
        Log.e(TAG, "handlingImage: " + widthScreen +" "+heightScreen);
        scaleDip = getResources().getDisplayMetrics().density;
        scaleLeftFrameThumnail = (float) 176 / (float) bitmapFrame.getWidth() * scaleDip;
        scaleTopFrameThumnail = (float) 40 / (float) bitmapFrame.getHeight() * scaleDip;
        scaleFrameBeginWithFixed = (float) (widthScreen) / (float) bitmapFrame.getWidth();
        widthFrameFixed = widthScreen;
        heightFrameFixed = (int) (scaleFrameBeginWithFixed * bitmapFrame.getHeight());
        bitmapFrameFixed = Bitmap.createScaledBitmap(bitmapFrame, widthFrameFixed, heightFrameFixed, false);
        bitmapBgFixed = Bitmap.createScaledBitmap(bitmapBg, widthFrameFixed, heightFrameFixed, false);
        bitmapEmptyBg = Bitmap.createBitmap(widthFrameFixed, heightFrameFixed, Bitmap.Config.ARGB_8888);
        marginLeftFrameThumnail = (int) (scaleLeftFrameThumnail * widthFrameFixed * 0.42) ;
        marginTopFrameThumnail = (int) (scaleTopFrameThumnail * heightFrameFixed * 0.42);
        widthBitmapFrameThumnailFixed = (int) (bitmapFrameThumnail.getWidth() * scaleFrameBeginWithFixed);
        heightBitmapFrameThumnailFixed = (int) (bitmapFrameThumnail.getHeight() * scaleFrameBeginWithFixed);
        bitmapFrameThumnailFixed = Bitmap.createScaledBitmap(bitmapFrameThumnail, widthBitmapFrameThumnailFixed, heightBitmapFrameThumnailFixed, false);
        Canvas mCanvasBg = new Canvas(bitmapEmptyBg);
        Paint mPaintBg = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintBg.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR.DST_OUT));
        mCanvasBg.drawBitmap(bitmapBgFixed, 0, 0, null);
        mCanvasBg.drawBitmap(bitmapFrameThumnailFixed, marginLeftFrameThumnail, marginTopFrameThumnail, mPaintBg);
        bitmapEmptyThumnailFixed = Bitmap.createBitmap(widthBitmapFrameThumnailFixed, heightBitmapFrameThumnailFixed, Bitmap.Config.ARGB_8888);
        bitmapBgThumnailFixed = Bitmap.createScaledBitmap(bitmapBg, widthBitmapFrameThumnailFixed, heightBitmapFrameThumnailFixed, false);
        Canvas mCanvasThumnail = new Canvas(bitmapEmptyThumnailFixed);
        Paint mPaintThumnail = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCanvasThumnail.drawBitmap(bitmapBgThumnailFixed, 0, 0, mPaintThumnail);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(marginLeftFrameThumnail, marginTopFrameThumnail, 0, 0);
        imgThumnail.setLayoutParams(layoutParams);
        imgFrame.setImageBitmap(bitmapFrameFixed);
        imgBg.setImageBitmap(bitmapEmptyBg);
        imgThumnail.setImageBitmap(bitmapEmptyThumnailFixed);
        imgThumnail.setZoom(1);
        imgThumnail.setOnTouchImageViewListener(new TouchImageView.OnTouchImageViewListener() {
            @Override
            public void onMove() {
                isZoomed = imgThumnail.isZoomed();
                imgThumnail.setDrawingCacheEnabled(true);
                imgThumnail.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                bitmapThumnailToSave = imgThumnail.getDrawingCache();
            }
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case REQUEST_PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    handleImage(data.getData());
                    try {
                        Uri selectedImage = data.getData();
                        InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                        bitmapBg = BitmapFactory.decodeStream(imageStream);
                        handlingImage();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    finish();
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.lnPIP:
                lnPIP.setBackgroundColor(Color.parseColor("#232935"));
                lnChangeBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterFG.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFrame.setBackgroundColor(Color.parseColor("#1f2530"));
                break;
            case R.id.lnFrame:
                lnFrame.setBackgroundColor(Color.parseColor("#232935"));
                lnChangeBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterFG.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnPIP.setBackgroundColor(Color.parseColor("#1f2530"));
                break;
            case R.id.lnFilterBg:
                lnFilterBg.setBackgroundColor(Color.parseColor("#232935"));
                lnChangeBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterFG.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFrame.setBackgroundColor(Color.parseColor("#1f2530"));
                lnPIP.setBackgroundColor(Color.parseColor("#1f2530"));
                lnListFilterType.setVisibility(View.VISIBLE);
                lnChangeBgAndFg.setVisibility(View.GONE);
                break;
            case R.id.lnFilterForeBg:
                lnFilterFG.setBackgroundColor(Color.parseColor("#232935"));
                lnChangeBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFrame.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnPIP.setBackgroundColor(Color.parseColor("#1f2530"));
                lnListFilterType.setVisibility(View.VISIBLE);
                lnChangeBgAndFg.setVisibility(View.GONE);
                break;
            case R.id.lnChangeBg:
                lnChangeBg.setBackgroundColor(Color.parseColor("#232935"));
                lnFrame.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterFG.setBackgroundColor(Color.parseColor("#1f2530"));
                lnFilterBg.setBackgroundColor(Color.parseColor("#1f2530"));
                lnPIP.setBackgroundColor(Color.parseColor("#1f2530"));
                lnListFilterType.setVisibility(View.GONE);
                lnChangeBgAndFg.setVisibility(View.VISIBLE);
                break;
//            case R.id.button_choose_filter:
//                GPUImageFilterTools.showDialog(this, new GPUImageFilterTools.OnGpuImageFilterChosenListener() {
//
//                    @Override
//                    public void onGpuImageFilterChosenListener(final GPUImageFilter filter) {
//                        switchFilterTo(filter);
//                        mGPUImageView.requestRender();
//                    }
//
//                });
//                break;
//            case R.id.button_save:
//                saveImage();
//                break;

            default:
                break;
        }

    }

    @Override
    public void onPictureSaved(final Uri uri) {
        Toast.makeText(this, "Saved: " + uri.toString(), Toast.LENGTH_SHORT).show();
    }

    private void saveImage() {
        String fileName = System.currentTimeMillis() + ".jpg";
        mGPUImageView.saveToPictures("GPUImage", fileName, this);
//        mGPUImageView.saveToPictures("GPUImage", fileName, 1600, 1600, this);
    }

    private void switchFilterTo(final GPUImageFilter filter) {
        if (mFilter == null
                || (filter != null && !mFilter.getClass().equals(filter.getClass()))) {
            mFilter = filter;
            mGPUImageView.setFilter(mFilter);
            mFilterAdjuster = new GPUImageFilterTools.FilterAdjuster(mFilter);

//            findViewById(R.id.seekBar).setVisibility(
//                    mFilterAdjuster.canAdjust() ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
        if (mFilterAdjuster != null) {
            mFilterAdjuster.adjust(progress);
        }
        mGPUImageView.requestRender();
    }
    @Override
    public void onStartTrackingTouch(final SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(final SeekBar seekBar) {
    }

    private void handleImage(final Uri selectedImage) {
        mGPUImageView.setImage(selectedImage);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.myFilterTypes.clear();
    }


}
