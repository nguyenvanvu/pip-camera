package com.fiwi.pipcamera.model;

import com.fiwi.pipcamera.adapter.FilterType;

/**
 * Created by WorkSpace on 6/29/2017.
 */

public class MyFilterType {
    public boolean check;
    public String name;
    public FilterType filter;

    public MyFilterType(boolean check, String name, FilterType filter) {
        this.check = check;
        this.name = name;
        this.filter = filter;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FilterType getFilter() {
        return filter;
    }

    public void setFilter(FilterType filter) {
        this.filter = filter;
    }

}
