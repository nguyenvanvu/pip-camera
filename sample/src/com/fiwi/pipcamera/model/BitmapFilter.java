package com.fiwi.pipcamera.model;

import android.graphics.Bitmap;

/**
 * Created by WorkSpace on 6/29/2017.
 */

public class BitmapFilter {
    private Bitmap bitmapFilter;
    private boolean check;

    public BitmapFilter(Bitmap bitmapFilter, boolean check) {
        this.bitmapFilter = bitmapFilter;
        this.check = check;
    }

    public Bitmap getBitmapFilter() {
        return bitmapFilter;
    }

    public void setBitmapFilter(Bitmap bitmapFilter) {
        this.bitmapFilter = bitmapFilter;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
